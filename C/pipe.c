#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BUFFER_SIZE 25
#define READ_END 0
#define WRITE_END 1

int main()
{
	char write_msg[BUFFER_SIZE] = "Greetings";
	char read_msg[BUFFER_SIZE];
	int fd[2];
	pid_t pid;

	/* create the pipe and check for errors */
	if ( pipe(fd) == -1 ) {
		fprintf(stderr, "Pipe failed\n");
		return 1;
	}
	pid = fork();
	if ( pid < 0 ) {
		fprintf(stderr, "Fork failed\n");
		return 1;
	} 
	
	if ( pid > 0 ) { /* this is the parent */
		/* closed unused end of pipe */
		close(fd[READ_END]);
		/* send to the pipe */
		write(fd[WRITE_END], write_msg, strlen(write_msg)+1);
		/* close the pipe */
		close(fd[WRITE_END]);
	} else { /* this is the child */
		/* closed unused end of pipe */
		close(fd[WRITE_END]);
		/* send to the pipe */
		read(fd[READ_END], read_msg, BUFFER_SIZE);
		printf("the child just read %s from the parent \n",read_msg);
		/* close the pipe */
		close(fd[READ_END]);
	}
	return 0;
}
		

		

