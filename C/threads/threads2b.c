#include <pthread.h> 
#include <stdio.h>
#include <stdlib.h>

int sum; /* this data is shared by the thread(s) */
void *runner(void *param); /* threads call this function */

int main(int argc, char *argv[])

{
	sum = 0; /* do this once */

	pthread_t tid1, tid2; /* the thread identifier */
	pthread_attr_t attr1, attr2; /* set of thread attributes */

	if (argc != 3) 
	{ 
		fprintf(stderr,"usage: a.out <integer value> <integer value>\n"); return -1; 
	}
	if (atoi(argv[1]) < 0) 
	{ 
		fprintf(stderr,"%d must be >= 0\n",atoi(argv[1])); return -1; 
	}

	/* get the default attributes */
	pthread_attr_init(&attr1);
	pthread_attr_init(&attr2);

	/* create the threads */
	pthread_create(&tid1,&attr1,runner,argv[1]);
	pthread_create(&tid2,&attr2,runner,argv[2]);

	/* wait for the thread to exit */
	pthread_join(tid1,NULL); 
	pthread_join(tid2,NULL); 
	printf("\nsum = %d\n",sum);
}

/* The thread will begin control in this function */ 
void *runner(void *param)
{ 
	int i;

	for (i = atoi(param); i > 0; i--) 
	{
		sum += i; 
		printf("i=%d\tsum=%d\n",i,sum);
	}
	pthread_exit(0); 
} 

