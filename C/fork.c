#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
	pid_t  pid;
	/* fork another process */
	pid = fork();
	if (pid < 0) { /* error occurred */
		fprintf(stderr, "Fork Failed");
		exit(-1);
	}
	else if (pid == 0) { /* child process */
		printf ("This is the child executing 'ls': \n");
		execlp("/bin/ls", "ls", NULL);
		printf ("The child is now ready to exit \n");
	}
	else { /* parent process */
		/* parent will wait for the child to complete */
		printf ("The Parent is about to wait\n");
		wait (NULL);
		printf ("Child Completed\n");
		exit(0);
	}
}

